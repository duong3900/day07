<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        #container-border{
            width: 600px;
            height: 800px;
            margin-left: 350px;
            border: solid #006ccb 2px;        
        }
        #top{
            width: 400px;
            height: 120px;
            margin-left: 100px;
            margin-top: 50px;
            border: solid #fff; 
        }
        .search{
            background-color: #006ccb;
            height: 40px;
            width: 100px;
            border-radius: 8px;
            margin-left: 100px;
            margin-top: 20px;
            color: white;
        }
        .bottom{
            width: 100%;
            height: 400px;
            margin-top: 20px;
             
        }
        .add{
            background-color: #006ccb;
            height: 40px;
            width: 100px;
            border-radius: 8px;
            margin-left: 450px;
            margin-top: 20px;
            color: white; 

        }
        .table2{
            border: none;
            margin-top: 30px;
            border-spacing: 25px;
        }
        .click{
            padding: 5px 10px;
            background-color: #006ccb;
            color: #fff;
            border: 2px solid #006ccb;
            
        }
        .selectbox{
            width: 170px;
        }
        
    </style>
</head>
<body>
    <div id="container-border">
    <form action='' method='POST'>
        <div id="top">
            <table>
                <tr>
                    <td>Khoa</td>
                    <td><select class='selectbox' name = 'Khoa' value = ''>
                        <?php
                            $khoa = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                            foreach ($khoa as $key => $value) {
                            echo "
                            <option ";
                            echo isset($_POST['Khoa']) && $_POST['Khoa'] == $key ? "selected " : "";
                            echo "  value='" . $key . "'>" . $value  . "</option>";
                            }
                            $_SESSION['Khoa'] = $khoa[$_POST['Khoa']];
                        ?>
                        </select></td>
                </tr>
                <tr>
                    <td>Từ Khóa</td>
                    <td>
                        <input id="textin" type="text">
                    </td>
                </tr>
            </table>
            <button class="search">Tìm Kiếm</button>
        </div>
        <div class="bottom">
            <p>Số sinh viên tìm thấy:XXX</p>
            <?php
                    if (isset($_POST["signup"]))
                    { 
                        header("Location: ../day07/signup.php");
                    }
                ?>
                <input class="add" type="submit" name='signup'  value="Thêm"></input>
            <div>
                <table class="table2">
                    <tr class="tr1">
                      <th><p>No</p></th>
                      <th><p>Tên sinh viên</p></th>
                      <th><p>Khoa</p></th>
                      <th>&emsp;&emsp; &emsp;</th>
                      <th><p>Action</p></th>
                      
                      
                      
                    </tr>
                    <tr class="tr2">
                      <td>1</td>
                      <td>Nguyễn Văn A</td>
                      <td>Khoa học máy tính</td>
                      <th>&nbsp;&nbsp;&nbsp;&nbsp;</th>
                      <td><button class="custom click">Xóa</button>
                        <button class="click">Sửa</button></td>
                    </tr>
                    <tr class="tr3">
                      <td>2</td>
                      <td>Trần Thị B</td>
                      <td>Khoa học máy tính</td>
                      <th>&nbsp;&nbsp;&nbsp;&nbsp;</th>
                      <td><button class="custom click">Xóa</button>
                        <button class="click">Sửa</button></td>
                    </tr>
                    <tr class="tr4">
                      <td>3</td>
                      <td>Nguyễn Hoàng C</td>
                      <td>Khoa học vật liệu</td>
                      <th>&nbsp;&nbsp;&nbsp;&nbsp;</th>
                      <td><button class="custom click">Xóa</button>
                        <button class="click">Sửa</button></td>
                    </tr>
                    <tr>
                      <td>4</td>
                      <td>Đinh Quang D</td>
                      <td>Khoa học vật liệu</td>
                      <th>&nbsp;&nbsp;&nbsp;&nbsp;</th>
                      <td><button class="custom click">Xóa</button>
                        <button class="click">Sửa</button></td>
                    </tr>
                  </table>
                </table>
            </div>
        </div>
    </div>
</body>
</html>