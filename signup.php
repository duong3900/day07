<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- CSS only -->
    <style>
        .frames {
            width: 600px;
            height: 550px;
            border: solid #006ccb 2px;
            margin-left: 30%;
            margin-top: 30px;
        }

        .frames-margin {
            margin-top: 50px;
            margin-left: 25px;
        }

        .custom-table {
            border-spacing: 25px;
            border-spacing: 15px;

        }

        .display {
            width: 150px;
            height: 35px;
            padding-left: 15px;
            border: solid #006ccb 2px;
            color: #fff;
        }

        .display2 {
            width: 230px;
            height: 35px;
            padding-left: 15px;
            border: solid #006ccb 2px;
            color: #000;
        }

        .display3 {
            width: 155px;
            height: 38px;
            padding-left: 15px;
            border: solid #006ccb 2px;
            color: #fff;
        }

        .checkbox {
            font-size: 25px;

        }

        .checkbox1 {
            width: 30px;
            height: 22px;
            background-color: aqua;
        }

        .checkbox2 {
            width: 30px;
            height: 22px;

        }

        .selectbox {
            width: 175px;
            height: 38px;
            padding-left: 15px;
            border: solid #006ccb 2px;

        }

        #signup {
            margin-left: 30%;

        }

        .customsignup {
            color: white;
            background-color: rgb(0, 162, 40);
            height: 50px;
            width: 140px;
            border: #006ccb;
            border-radius: 10px;
            font-size: 20px;
        }

        .customvalidate {
            color: red;
        }
    </style>

</head>
<?php
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
    session_start();
    $_SESSION = $_POST;
    $error = array();
    $allowed_exttension = array('gif', 'png', 'jpg','jpeg');
    $hinhanh = basename($_FILES['hinhanh']['name']);
    $file_exttension = pathinfo($hinhanh, PATHINFO_EXTENSION);

        if (empty(trim($_POST['fullname']))) {
            $error['fullname'] = 'Hãy nhập tên';
        } else {
            $username = $_POST['fullname'];
        }

        if (empty($_POST['gender'])) {
            $error['gender'] = 'Hãy chọn giới tính';
        } else {
            $gender = $_POST['gender'];
        }
        if (empty($_POST['faculty'])) {
            $error['faculty'] = 'Hãy chọn phân khoa';
        } else {
            $faculty = $_POST['faculty'];
        }
        if (empty($_POST['date'])) {
            $error['date'] = 'Hãy nhập ngày sinh';
        } else {
            $date = $_POST['date'];
            if (!check($_POST['date'])) {
                $error['date'] = "Hãy nhập ngày sinh đúng định dạng";
            }
        }
        if(!in_array($file_exttension, $allowed_exttension)){
            $error['hinhanh'] = "Chọn file ảnh gồm các định dạng jpeg, gif, png, jpg"; 
    }
    
    
        
        if (empty($error)) {
            header("Location: http://localhost/web/day06/submit.php");
        }
    }

    // check fomat date dd/mm/yyyy
    function check($data)
    {
        $reg = "/^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[0-2])\/(19|20)\d{2}$/";
        if (preg_match($reg, $data)) {
            return true;
        } else {
            return false;
        }
    }




    ?>
<body>

    <form action='' method='POST' enctype='multipart/form-data'>
    
        <div class="frames">
            <div class='frames-margin'>
                <div style=' margin-left: 20px; width: 450px;'>
                </div>

                <table class='custom-table'>
                    <tr>
                        <td class='display' style='background-color: #1bb857;'>Họ và tên <label class="customvalidate" for="">*</label></td>

                        <td>
                            <input class="display2" type="text" name="fullname" id="fullname" value="<?php isset($_POST['fullname']) ? $_POST['fullname'] : ''; ?>" />

                        </td>
                        <span style='color: red;'><?php echo isset($error['fullname']) ? $error['fullname'] : '';

                                                    ?>
                            <br>
                        </span>
                    </tr>

                    <tr>
                        <td class='display' style='background-color: #1bb857;'>Giới tính <label class="customvalidate" for="">*</label></td>
                        <td>
                            <?php
                            $gioitinh = array(0 => 'Nam', 1 => 'Nữ');
                            for ($i = 0; $i <= count($gioitinh) - 1; $i++) {
                                echo "
                                <input class='checkbox1' type='radio' id='' class='gender' name='gender' value='$gioitinh[$i]'> " . $gioitinh[$i];
                            }

                            ?>

                        </td>
                        <span style='color: red;'>
                            <?php
                            echo isset($error['gender']) ? $error['gender'] : '';
                            ?>
                            <br>
                        </span>

                    </tr>

                    <tr>
                        <td class='display' style='background-color: #1bb857;'>Phân khoa <label class="customvalidate" for="">*</label></td>
                        <td>
                            <select name='faculty' class='selectbox'>
                                <?php
                                $faculty = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                                foreach ($faculty as $key => $value) {
                                    echo "
                                    <option value='" . $key . "'>" . $value . "</option>";
                                }
                                $_SESSION['faculty'] = $faculty[$_POST['faculty']];

                                ?>
                            </select>
                        </td>
                        <span style='color: red;'>
                            <?php echo isset($error['faculty']) ? $error['faculty'] : '';

                            ?>
                            <br>
                        </span>

                    </tr>
                    <tr>
                        <td class='display' style='background-color: #1bb857;'>Ngày sinh <label class="customvalidate" for="">*</label></td>
                        <td><input class='display3' name='date' placeholder='dd/mm/yyyy' type='text' value=''>

                        </td>



                        </td>
                        <span style='color: red;'>
                            <?php echo isset($error['date']) ? $error['date'] : '';
                            
                            ?>
                            <br>
                    </tr>

                    <tr>
                        <td class='display' style='background-color: #1bb857;'>Địa chỉ</td>
                        <td><input class='display2' type='text' name='address'></td>
                    </tr>
                    <tr>
                        <td class='display' style='background-color: #1bb857;'>Hình Ảnh</td>
                        <td><input class='' type='file' accept="image/*" name='hinhanh' value=''>



                            <?php
                            
                            if (isset($_POST['submit']) && ($_POST['submit'])) {
                                $target_dir = "uploads/";
                                if(!file_exists($target_dir)){
                                    mkdir($target_dir);
                                 }
                                $extension_pos = strrpos($hinhanh, '.');
                                 $thumb = substr($hinhanh, 0, $extension_pos) . '_' .date("YmdHis") . substr($hinhanh, $extension_pos);
                                 
                                $hinhanh = basename($_FILES['hinhanh']['name']);
                                $target_file = $target_dir . $thumb;
                                $_SESSION['hinhanh'] = $target_file;
                            
                                if(in_array($file_exttension, $allowed_exttension)){
                                move_uploaded_file($_FILES["hinhanh"]["tmp_name"], $target_file);
                            }    
                        }
                            ?>
                            
                        </td>
                        <span style='color: red;'>
                            <?php echo isset($error['hinhanh']) ? $error['hinhanh'] : '';

                            ?>
                            <br>
                        </span>
                    </tr>


                </table>

                <div id="signup">
                    <td><input type="submit" name="submit" value="Đăng ký" class="customsignup" /></td>
                </div>
            </div>
</body>

</html>